package com.refafacil.zdp.refafacilv2.Utils;

import android.content.Context;

import com.refafacil.zdp.refafacilv2.items.Carrito;
import com.refafacil.zdp.refafacilv2.items.Categoria;
import com.refafacil.zdp.refafacilv2.items.Marca;
import com.refafacil.zdp.refafacilv2.items.Modelo;
import com.refafacil.zdp.refafacilv2.items.Pieza;
import com.refafacil.zdp.refafacilv2.items.Producto;
import com.refafacil.zdp.refafacilv2.items.Taller;

import java.util.ArrayList;

public class Globals {
    Context context;
    public ArrayList<Carrito> listCarrito;
    public ArrayList<Categoria>listCategorias;
    public ArrayList<Marca>listMarcas;
    public ArrayList<Modelo>listModelos;
    public ArrayList<Pieza>listPieza;
    public ArrayList<Producto>listaProductos;
    public ArrayList<Taller>listTaller;

    public Globals(Context context){
        this.context = context;
        listCarrito = new ArrayList<>();
        listCategorias = new ArrayList<>();
        listMarcas = new ArrayList<>();
        listModelos = new ArrayList<>();
        listPieza = new ArrayList<>();
        listaProductos = new ArrayList<>();
        listTaller = new ArrayList<>();
        llenarListas();
    }
    public void llenarListas(){
        listCategorias.add(new Categoria(1,"AMORTIGUADORES",1));
        listCategorias.add(new Categoria(2,"CERRADURAS",1));
        listCategorias.add(new Categoria(3,"CHICOTES Y CABLES",1));
        listCategorias.add(new Categoria(4,"CILINDROS AUTOMOTRICES",1));
        listCategorias.add(new Categoria(5,"CONTROLES ELECTRICOS",1));
        listCategorias.add(new Categoria(6,"DEPOSITOS",1));
        listCategorias.add(new Categoria(7,"ELEVADORES",1));
        listCategorias.add(new Categoria(8,"MANIJAS",1));
        listCategorias.add(new Categoria(9,"NIVELADORES",1));
        listCategorias.add(new Categoria(10,"PERNOS",1));
        listCategorias.add(new Categoria(11,"QUIMICOS",1));
        listCategorias.add(new Categoria(12,"REFACCIONES VARIAS",1));
        listCategorias.add(new Categoria(13,"TAPONES",1));
        listCategorias.add(new Categoria(14,"TIRANTES",1));

        listMarcas.add(new Marca(1,"Renault",1));
        listMarcas.add(new Marca(2,"Ford",1));
        listMarcas.add(new Marca(3,"Chevrolet",1));
        listMarcas.add(new Marca(4,"Nissan",1));
        listMarcas.add(new Marca(5,"Volkswagen",1));
        listMarcas.add(new Marca(6,"Honda",1));
        listMarcas.add(new Marca(7,"Hyundai",1));
        listMarcas.add(new Marca(8,"Dodge",1));

        listModelos.add(new Modelo(1,"Stepway",1,1));
        listModelos.add(new Modelo(2,"Logan",1,1));
        listModelos.add(new Modelo(3,"Duster",1,1));
        listModelos.add(new Modelo(4,"Captur",1,1));
        listModelos.add(new Modelo(5,"Koleos",1,1));
        listModelos.add(new Modelo(6,"Orosh",1,1));

        listModelos.add(new Modelo(7,"Figo",1,2));
        listModelos.add(new Modelo(8,"Fiesta",1,2));
        listModelos.add(new Modelo(9,"Focus",1,2));
        listModelos.add(new Modelo(10,"Fusion",1,2));
        listModelos.add(new Modelo(11,"Ecosport",1,2));
        listModelos.add(new Modelo(12,"Explorer",1,2));

        listModelos.add(new Modelo(13,"Beat",1,3));
        listModelos.add(new Modelo(14,"Sperk",1,3));
        listModelos.add(new Modelo(15,"Aveo",1,3));
        listModelos.add(new Modelo(16,"Fusion",1,3));
        listModelos.add(new Modelo(17,"Cavalier",1,3));
        listModelos.add(new Modelo(18,"Blazer",1,3));

        listModelos.add(new Modelo(19,"Altima",1,4));
        listModelos.add(new Modelo(20,"Sentra",1,4));
        listModelos.add(new Modelo(21,"Note",1,4));
        listModelos.add(new Modelo(22,"Versa",1,4));
        listModelos.add(new Modelo(23,"March",1,4));
        listModelos.add(new Modelo(24,"Armada",1,4));

        listModelos.add(new Modelo(25,"Jetta",1,5));
        listModelos.add(new Modelo(26,"Bettle",1,5));
        listModelos.add(new Modelo(27,"Gol",1,5));
        listModelos.add(new Modelo(28,"Vento",1,5));
        listModelos.add(new Modelo(29,"Golf",1,5));
        listModelos.add(new Modelo(30,"Caddy",1,5));

        listModelos.add(new Modelo(31,"City",1,6));
        listModelos.add(new Modelo(32,"Civic",1,6));
        listModelos.add(new Modelo(33,"Accord",1,6));
        listModelos.add(new Modelo(34,"Fit",1,6));
        listModelos.add(new Modelo(35,"Pilot",1,6));
        listModelos.add(new Modelo(36,"Cr-v",1,6));

        listModelos.add(new Modelo(37,"Elantra",1,7));
        listModelos.add(new Modelo(38,"Grand i10",1,7));
        listModelos.add(new Modelo(39,"Accent",1,7));
        listModelos.add(new Modelo(40,"Ionic",1,7));
        listModelos.add(new Modelo(41,"Creat",1,7));
        listModelos.add(new Modelo(42,"Sterak",1,7));

        listModelos.add(new Modelo(43,"Charter",1,8));
        listModelos.add(new Modelo(44,"Durango",1,8));
        listModelos.add(new Modelo(45,"Actitude",1,8));
        listModelos.add(new Modelo(46,"Neon",1,8));
        listModelos.add(new Modelo(47,"Journey",1,8));
        listModelos.add(new Modelo(48,"Grand Caravan",1,8));

        listPieza.add(new Pieza(1,"AMORTIGUADOR PARA COFRE DE GAS CHEVROLET Astra Mod. 04-11 L=56.5 CIL=31.5 cm",1,1,1));
        listPieza.add(new Pieza(2,"AMORTIGUADOR PARA COFRE DE GAS CHEVROLET Camaro Mod. 98-02 L=61 CIL=30.5 cm.",1,1,1));
        listPieza.add(new Pieza(3,"AMORTIGUADOR PARA COFRE DE GAS CHEVROLET Chevy C3 Mod. 09-12.",1,1,1));
        listPieza.add(new Pieza(4,"AMORTIGUADOR PARA COFRE DE GAS CHEVROLET Grand Prix Mod. 00-03 sin spoiler.",1,1,1));
        listPieza.add(new Pieza(5,"AMORTIGUADOR PARA COFRE DE GAS DODGE Camioneta Ram Mod. 02-07 L=50.4 CIL=26.5 cm 43.5 kg.",1,1,1));
        listPieza.add(new Pieza(6,"AMORTIGUADOR PARA COFRE DE GAS DODGE Dakota Mod.05-09 L=51.3 CIL=24.8 cm.",1,1,1));
        listPieza.add(new Pieza(7,"AMORTIGUADOR PARA COFRE DE GAS CHRYSLER Intrepid-Concorde Mod. 93-97 L=35.5 CIL=22 cm.",1,1,1));
        listPieza.add(new Pieza(8,"AMORTIGUADOR PARA COFRE DE GAS FORD Expedition 97-06 - F150-250 Mod.97-04 * 47.9 kg* L=36 CIL=22 cm.",1,1,1));
        listPieza.add(new Pieza(9,"AMORTIGUADOR PARA COFRE DE GAS FORD Grand Marquis 92-97 - Town Car 98-02 54 kg. * L=38.5 CIL=26 cm",1,1,1));
        listPieza.add(new Pieza(10,"AMORTIGUADOR PARA COFRE DE GAS FORD Grand Marquis - Crown Victoria Mod. 98-08 * 58 kg. * L=36 CIL=20 cm",1,1,1));
        listPieza.add(new Pieza(11,"AMORTIGUADOR PARA COFRE DE GAS FORD Expedition Mod. 03-06 * L=35.6 CIL=21 cm.",1,1,1));
        listPieza.add(new Pieza(12,"AMORTIGUADOR PARA COFRE DE GAS FORD Explorer Mod. 02-09 * 26.5 kg. * L=35.2 CIL=19.8 cm.",1,1,1));
        listPieza.add(new Pieza(13,"AMORTIGUADOR PARA COFRE DE GAS FORD Excursion Mod. 00-05 F250 F350 F450 F550 Mod. 99-06.",1,1,1));

        listPieza.add(new Pieza(14,"CERRADURA COFRE CHEVROLET Camioneta C10 Mod. 73-91 Blazer Mod. 95-01",1,1,2));
        listPieza.add(new Pieza(15,"CERRADURA COFRE CHEVROLET Aveo Mod. 04-11 Pontiac G3 Mod. 07-11",1,1,2));
        listPieza.add(new Pieza(16,"CERRADURA COFRE CHEVROLET Corsa Mod. 02-08",1,1,2));
        listPieza.add(new Pieza(17,"CERRADURA COFRE CHEVROLET Malibu Mod. 04-07 Malibu G6 Mod. 05-09",1,1,2));
        listPieza.add(new Pieza(18,"CERRADURA COFRE CHEVROLET Malibu Mod. 08-12",1,1,2));
        listPieza.add(new Pieza(19,"CERRADURA COFRE CHEVROLET Optra Mod. 06-10",1,1,2));
        listPieza.add(new Pieza(20,"CERRADURA COFRE CHEVROLET Camioneta Sierra Mod. 03-06",1,1,2));
        listPieza.add(new Pieza(21,"CERRADURA COFRE CHEVROLET Camioneta Mod. 07-13 Sierra-Avalanche-Escalade Mod 07-13",1,1,2));
        listPieza.add(new Pieza(22,"CERRADURA COFRE DODGE Camiontea Mod. 81-91",1,1,2));
        listPieza.add(new Pieza(23,"CERRADURA COFRE DODGE Camioneta Mod. 94-02",1,1,2));
        listPieza.add(new Pieza(24,"CERRADURA COFRE FORDD Focus Mod. 04-07",1,1,2));
        listPieza.add(new Pieza(25,"CERRADURA COFRE FORD Expedition Mod. 03-06",1,1,2));
        listPieza.add(new Pieza(26,"CERRADURA COFRE FORD Camioneta Mod. 77-84 Ranger Mod. 93-94",1,1,2));
        listPieza.add(new Pieza(27,"CERRADURA COFRE HONDA Accord Mod. 98-02",1,1,2));

        listPieza.add(new Pieza(28,"CHICOTE CERRADURA COFRE CHEVROLET Chevy 93-08 con jaladera (1.50 mts.)",1,1,3));
        listPieza.add(new Pieza(29,"CHICOTE CERRADURA COFRE CHEVROLET Suburban Mod. 97-06 (2.14 mts.)",1,1,3));
        listPieza.add(new Pieza(30,"CHICOTE CERRADURA COFRE CHEVROLET S10 Mod. 96- (1.84 mts.)",1,1,3));
        listPieza.add(new Pieza(31,"CHICOTE CERRADURA COFRE CHEVROLET S10 Mod. 96- (1.84 mts.)",1,1,3));
        listPieza.add(new Pieza(32,"CHICOTE CERRADURA COFRE CHEVROLET Spark Mod. 11-14 (1.54 mts.)",1,1,3));
        listPieza.add(new Pieza(33,"CHICOTE CERRADURA COFRE CHEVROLET S10 y Tracker Mod. 99-06 (1.8 mts.)",1,1,3));
        listPieza.add(new Pieza(34,"CHICOTE CERRADURA COFRE CHEVROLET Aveo Mod. 04-11 (1.76 mts.)",1,1,3));
        listPieza.add(new Pieza(35,"CHICOTE CERRADURA COFRE CHEVROLET Optra (1.86 mts.)",1,1,3));
        listPieza.add(new Pieza(36,"CHICOTE CERRADURA COFRE CHEVROLET Corsa con jaladera (1.70 mts.)",1,1,3));
        listPieza.add(new Pieza(37,"CABLE DE EMBRAGUE CHEVROLET Pop Mod. 94-96 (0.73 mts.)",1,1,3));
        listPieza.add(new Pieza(38,"CABLE DE EMBRAGUE CHEVROLET Chevy-Corsa-Monza Mod. 96-11 (0.83 mts.)",1,1,3));
        listPieza.add(new Pieza(39,"CABLE DE EMBRAGUE CHEVROLET Matiz G1 Mod. 04-05 (0.85 mts.)",1,1,3));
        listPieza.add(new Pieza(40,"CABLE DE EMBRAGUE CHEVROLET Matiz G2 Mod. 06-12 (0.83 mts.)",1,1,3));
        listPieza.add(new Pieza(41,"CABLE DE EMBRAGUE CHEVROLET Spark G1 Mod. 09-15 (0.93 mts.)",1,1,3));

        listPieza.add(new Pieza(42,"CILINDROS PUERTAS CHEVROLET Auto - Camioneta Mod. 63-96",1,1,4));
        listPieza.add(new Pieza(43,"CILINDROS PUERTAS CHEVROLET Camioneta mod. 87-97",1,1,4));
        listPieza.add(new Pieza(44,"CILINDROS PUERTAS CHEVROLET Silhoutte Mod. 97-04 (Cola gruesa) Sunfire Mod. 96-05 Montana Mod. 99-06",1,1,4));
        listPieza.add(new Pieza(45,"CILINDROS PUERTAS CHEVROLET Camioneta - Suburban - GMC 95-99",1,1,4));
        listPieza.add(new Pieza(46,"CILINDROS PUERTAS CHEVROLET Silverado-Sierra Mod. 99-00 Suburban-Tahoe 00-06",1,1,4));
        listPieza.add(new Pieza(47,"CILINDROS PUERTAS CHEVROLET Chevy C1 -Tornado Mod. 04-12 REPUESTOS con llave",1,1,4));
        listPieza.add(new Pieza(48,"CILINDROS PUERTAS CHEVROLET Matiz",1,1,4));
        listPieza.add(new Pieza(49,"CILINDRO PUERTA CHEVROLET Chevy con base metalica DERERECHO",1,1,4));

        listPieza.add(new Pieza(50,"CONTROL Elevador Elec. CHEVROLET Venture-Silhouette 97-05 * 7 Pin 1 Tecla",1,1,5));
        listPieza.add(new Pieza(51,"CONTROL Elevador Elec. CHEVROLET Suburban 83-91 Cam. 85-86 Blazer 84-91 Cutlass 78-88 * 5 Pin 1 Tecla.",1,1,5));
        listPieza.add(new Pieza(52,"CONTROL Elevador Elec. CHEVROLET Astro-Safari Mod. 96-05 * 5 Pin 1 Tecla.",1,1,5));
        listPieza.add(new Pieza(53,"CONTROL Elevador Elec. CHEVROLET Corsa-Astra-Tornado Mod. 00-08 * 4 Pin * 1 Tecla.",1,1,5));
        listPieza.add(new Pieza(54,"CONTROL Elevador Elec. CHEVROLET Suburban-Tahoe Escalade 03-06 Silverado 03-07 * 10+12+10+26+4 Pin 2 Tec..",1,1,5));
        listPieza.add(new Pieza(55,"CONTROL Elevador Elec. CHEVROLET Captiva Mod. 06-10 * 6 Pin 1 Tecla",1,1,5));

        listPieza.add(new Pieza(56,"DEPOSITO ANTICONGELANTE CHEVROLET Matiz G1 Mod. 2003-2005",1,1,6));
        listPieza.add(new Pieza(57,"DEPOSITO ANTICONGELANTE CHEVROLET Matiz G2 Mod. 2006-2010",1,1,6));
        listPieza.add(new Pieza(58,"DEPOSITO ANTICONGELANTE CHEVROLET Optra Mod. 06-10",1,1,6));
        listPieza.add(new Pieza(59,"DEPOSITO ANTICONGELANTE CHEVROLET Aveo Mod. 06-10 PONTIAC G3 Mod. 06-09",1,1,6));
        listPieza.add(new Pieza(60,"DEPOSITO ANTICONGELANTE CHEVROLET Chevy Mod. 94-12 1.6 litros * con tapon",1,1,6));
        listPieza.add(new Pieza(61,"DEPOSITO ANTICONGELANTE CHEVROLET G6 Mod. 05-10 CUELLO * con tapon",1,1,6));
        listPieza.add(new Pieza(62,"DEPOSITO ANTICONGELANTE CHEVROLET Grand Prix-Century-Regal Mod. 97-98 3.1 litros-3.8 litros * con tapa",1,1,6));
        listPieza.add(new Pieza(63,"DEPOSITO ANTICONGELANTE CHEVROLET Aveo Mod. 11-17 Pontiac G3",1,1,6));

        listPieza.add(new Pieza(64,"ELEVADOR CHEVROLET Auto Chevy Mod. 93-12 2 Ptas. IZQ",1,1,7));
        listPieza.add(new Pieza(65,"ELEVADOR CHEVROLET Auto Chevy Mod. 93-12 2 Ptas. DER",1,1,7));
        listPieza.add(new Pieza(66,"ELEVADOR CHEVROLET Auto Chevy Mod. 93-06 4 Ptas. Delantero IZQ",1,1,7));
        listPieza.add(new Pieza(67,"ELEVADOR CHEVROLET Auto Chevy Mod. 93-06 4 Ptas. Delantero DER",1,1,7));
        listPieza.add(new Pieza(68,"ELEVADOR FORD Fiesta Mod. 99-02 IZQ",1,1,7));
        listPieza.add(new Pieza(69,"ELEVADOR FORD Fiesta Mod. 99-02 DER",1,1,7));
        listPieza.add(new Pieza(70,"ELEVADOR FORD Ka Mod. 01-07 IZQ",1,1,7));
        listPieza.add(new Pieza(71,"ELEVADOR VW Jetta A2 Mod. 88-92 delantero IZQ",1,1,7));
        listPieza.add(new Pieza(72,"ELEVADOR VW Jetta A2 Mod. 88-92 delantero DER",1,1,7));

        listPieza.add(new Pieza(73,"MANIJA EXTERIOR CHEVROLET Chevy Monza-Astra Delantera Mod. 94-09 IZQUIERDO",1,1,8));
        listPieza.add(new Pieza(74,"MANIJA EXTERIOR CHEVROLET Chevy Monza-Astra Trasera Mod. 94-09 DERECHO",1,1,8));
        listPieza.add(new Pieza(75,"MANIJA EXTERIOR CHEVROLET Luv - Isuzu Mod. 88-95 IZQUIERDO",1,1,8));
        listPieza.add(new Pieza(76,"MANIJA EXTERIOR CHEVROLET Luv - Isuzu Mod. 88-95 DERECHO",1,1,8));
        listPieza.add(new Pieza(77,"MANIJA EXTERIOR CHEVROLET Camioneta Kodiak Mod. 95-03 IZQUIERDO",1,1,8));
        listPieza.add(new Pieza(78,"MANIJA EXTERIOR CHEVROLET Camioneta Kodiak Mod. 95-03 DERECHO",1,1,8));
        listPieza.add(new Pieza(79,"MANIJA EXTERIOR CHEVROLET Trail Blazer-Bravada-Envoy Mod. 02-08 Delantera IZQUIERDO",1,1,8));

        listPieza.add(new Pieza(80,"NIVELADOR DE LUZ CHEVROLET Mod. 75-95 (10 piezas)",1,1,9));
        listPieza.add(new Pieza(81,"NIVELADOR DE LUZ CHEVROLET (10 piezas)",1,1,9));
        listPieza.add(new Pieza(82,"NIVELADOR DE LUZ FORD Mod. 69-79 (10 piezas)",1,1,9));
        listPieza.add(new Pieza(83,"NIVELADOR DE LUZ NISSAN Mod. 68-84 (10 piezas)",1,1,9));
        listPieza.add(new Pieza(84,"NIVELADOR DE LUZ TOYOTA (10 piezas)",1,1,9));
        listPieza.add(new Pieza(85,"NIVELADOR DE LUZ VOLKSWAGEN Atlantic (10 piezas)",1,1,9));

        listPieza.add(new Pieza(86,"PERNO BISAGRA PUERTA c/ bujes CHEV-DODGE Mod. 82-94 * 10.8 cm. ØB=7/16\" (10 pzas)",1,1,10));
        listPieza.add(new Pieza(87,"PERNO BISAGRA PUERTA c/ bujes CHEV Auto 8.1 cm. ØB=7/16\" (10 pzas)",1,1,10));
        listPieza.add(new Pieza(88,"PERNO BISAGRA PUERTA c/ bujes CHEV Mod. 70 * 9 cm. (10 pzas)",1,1,10));
        listPieza.add(new Pieza(89,"PERNO BISAGRA PUERTA c/ bujes CHEV S-10 * 10.9 cm. ØB=1/2\" (10 pzas)",1,1,10));

        listPieza.add(new Pieza(90,"ADHESIVO PARA PLACA ESPEJOS INTERIORES (Primer y adhesivo) 15 ml. de c/u",1,1,11));
        listPieza.add(new Pieza(91,"SELLADOR DE URETANO NPT U-SEAL 207 Salchicha de 600 ml. Secado normal (Prod. USA)",1,1,11));
        listPieza.add(new Pieza(92,"SELLADOR DE URETANO NPT U-SEAL 201 Cartucho de 310 ml. Secado rapido (Prod. USA)",1,1,11));
        listPieza.add(new Pieza(93,"SELLADOR DE URETANO NPT U-SEAL 201 Salchicha de 600 ml. Secado rapido (Prod. USA)",1,1,11));
        listPieza.add(new Pieza(94,"RESTAURADOR Y LIMPIADOR DE FAROS-CALAVERAS Frasco de 120 gms.",1,1,11));
        listPieza.add(new Pieza(95,"LIMPIADOR Y RESTAURADOR DE TAPICERIAS DE PIEL Frasco de 250 ml.",1,1,11));
        listPieza.add(new Pieza(96,"LIMPIADOR PARA CARBURADOR HIUK en Aerosol 340 ml.",1,1,11));
        listPieza.add(new Pieza(97,"SILICON ABRILLANTADOR ORIGINAL HIUK en Aerosol 255 ml.",1,1,11));
        listPieza.add(new Pieza(98,"PROTECTOR ANTICORROSIVO HIUK en Aerosol 354 cm3.",1,1,11));
        listPieza.add(new Pieza(99,"ACEITE AFLOJATODO HIUK en Aerosol 120 ml.",1,1,11));
        listPieza.add(new Pieza(100,"ACEITE LUBRICANTE HIUK en Aerosol de 120 ml.",1,1,11));
        listPieza.add(new Pieza(101,"PINTURA DE ALTA TEMPERATURA COLOR ALUMINIO HIUK en Aerosol 400 ml.",1,1,11));

        listPieza.add(new Pieza(103,"GUIA PARA CRISTAL CHEV. Cavalier Mod. 82-94 (5 pzas).",1,1,12));
        listPieza.add(new Pieza(104,"GUIA PARA CRISTAL CHEV. Cavalier Mod. 82-94 (larga) (5 pzas).",1,1,12));
        listPieza.add(new Pieza(105,"GUIA PARA CRISTAL CHEV. Celebrity-Cam. Mod. 80-94 (5 pzas).",1,1,12));
        listPieza.add(new Pieza(106,"GUIA PARA CRISTAL DODGE con perno aluminio (5 pzas).",1,1,12));
        listPieza.add(new Pieza(107,"CUENCA TAPA TRASERA FORD Camta. (2 pzas. Izq. y der.).",1,1,12));
        listPieza.add(new Pieza(108,"CUENCA VARILLA BRAZO LIMPIABRISAS DODGE 00-08 gde. (10 pzas.)",1,1,12));
        listPieza.add(new Pieza(109,"CUENCA VARILLA BRAZO LIMPIABRISAS NISSAN Grande (10 pzas.)",1,1,12));
        listPieza.add(new Pieza(110,"BOTAGUAS TOYOTA Cam. Vigo Mod. 05-12 Largo 69 cm.Exterior Trasero DER",1,1,12));
        listPieza.add(new Pieza(111,"BOTAGUAS HYUNDAI Atos Mod. 97-05 Delantero Largo 94 cm. Exterior IZQ",1,1,12));
        listPieza.add(new Pieza(112,"BOTAGUAS HYUNDAI Atos Mod. 97-05 Delantero Largo 94 cm. Exterior DER",1,1,12));
        listPieza.add(new Pieza(113,"TORNILLO PARA SEGURO DE VENTANILLA UNIVERSAL Negro (Boton)",1,1,12));

        listPieza.add(new Pieza(114,"TAPON GAS CHEVROLET Chevys-Cta.-C1-C2-Corsa-Astra-Vectra-Tigra-Optra 94-09 (CE)",1,1,13));
        listPieza.add(new Pieza(115,"TAPON GAS Ford Fiesta, Ka , Ikon, Courier, Escort (Importacion)",1,1,13));
        listPieza.add(new Pieza(116,"TAPON GAS Ford Fiesta 02-09, Ka , Courier, Ecosport, Focus (Importacion)",1,1,13));
        listPieza.add(new Pieza(117,"TAPON GAS Ford Camta-Windstar-Lobo-Explorer-Escape-Triton-Ranger 99-09 (Femsa)",1,1,13));
        listPieza.add(new Pieza(118,"TAPON GAS FORD 60-97/ John Deere (CE)",1,1,13));
        listPieza.add(new Pieza(119,"TAPON GAS VOLKSWAGEN 77-89 Caribe - Atlantic - Corsar (CE)",1,1,13));
        listPieza.add(new Pieza(120,"TAPON GAS VOLKSWAGEN Combi Mod. 73-02 (CE)",1,1,13));

        listPieza.add(new Pieza(121,"TIRANTE TAPA TRASERA CHEVROLET Suburban-Yukon Mod. 88-00 Camioneta 92-98 (39 cm.)",1,1,14));
        listPieza.add(new Pieza(122,"TIRANTE TAPA TRASERA CHEVROLETSilver-Sierra 99-06 Avalanche 02-06 (40.5 cm.)",1,1,14));
        listPieza.add(new Pieza(123,"TIRANTE TAPA TRASERA CHEVROLET S10-Sonoma-Camta Mod. 94-01 (47.5 cm.) IZQUIERDO",1,1,14));
        listPieza.add(new Pieza(124,"TIRANTE TAPA TRASERA CHEVROLET Suburban Mod. 78-91 (52 cm.)",1,1,14));
        listPieza.add(new Pieza(125,"TIRANTE TAPA TRASERA CHEVROLET Blazer-Jimmy Mod. 73-93 (58.5 cm.)",1,1,14));
        listPieza.add(new Pieza(126,"TIRANTE TAPA TRASERA CHEVROLET S10 Mod. 83-93 Chevy Mod. 99-03 (46 cm.)",1,1,14));
        listPieza.add(new Pieza(127,"TIRANTE TAPA TRASERA CHEVROLET Avalanche Mod. 07-09 IZQUIERDO (46 cm.)",1,1,14));



        listaProductos.add(new Producto("",1));

        listTaller.add(new Taller(1,"",3));

    }

}
