package com.refafacil.zdp.refafacilv2.items;

public class Marca {
    String Nombre;
    int Id,Imagen;
    public Marca(int Id,String Nombre,int Imagen){
        this.Id = Id;
        this.Nombre = Nombre;
        this.Imagen = Imagen;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public int getImagen() {
        return Imagen;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
