package com.refafacil.zdp.refafacilv2.items;

public class Pieza {
    String Nombre;
    int Id,Id_Modelo,Imagen,id_categoria;

    public Pieza(int Id,String Nombre,int Imagen,int Id_Modelo, int id_categoria){
        this.Id = Id;
        this.Nombre = Nombre;
        this.Imagen = Imagen;
        this.Id_Modelo = Id_Modelo;
        this.id_categoria = id_categoria;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNombre() {
        return Nombre;
    }

    public int getImagen() {
        return Imagen;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId_Modelo() {
        return Id_Modelo;
    }

    public void setId_Modelo(int id_Modelo) {
        Id_Modelo = id_Modelo;
    }

    public int getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(int id_categoria) {
        this.id_categoria = id_categoria;
    }
}
