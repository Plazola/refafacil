package com.refafacil.zdp.refafacilv2.events;

public interface ItemEvent {
    void onObjectListEvent(Object obj);
}
