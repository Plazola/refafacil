package com.refafacil.zdp.refafacilv2.items;

public class Categoria {
    int imagen,id;
    String nombre;

    public  Categoria(int id,String nombre,int imagen){
        this.id=id;
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public int getImagen() {
        return imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

