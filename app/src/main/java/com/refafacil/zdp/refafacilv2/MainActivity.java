package com.refafacil.zdp.refafacilv2;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.refafacil.zdp.refafacilv2.Utils.Globals;
import com.refafacil.zdp.refafacilv2.events.ItemEvent;
import com.refafacil.zdp.refafacilv2.fragments.Busqueda;
import com.refafacil.zdp.refafacilv2.fragments.CarritoFragment;
import com.refafacil.zdp.refafacilv2.fragments.Catalogo;
import com.refafacil.zdp.refafacilv2.fragments.MapsFragment;
import com.refafacil.zdp.refafacilv2.fragments.PerfilFragment;
import com.refafacil.zdp.refafacilv2.fragments.ProductoFragment;

public class MainActivity extends AppCompatActivity implements
        Catalogo.OnFragmentInteractionListener,
        CarritoFragment.OnFragmentInteractionListener,
        PerfilFragment.OnFragmentInteractionListener,
        ProductoFragment.OnFragmentInteractionListener,
        MapsFragment.OnFragmentInteractionListener,
        ItemEvent
{

    public FragmentManager manager;
    public FragmentTransaction transaction;
    Catalogo frCatalogo;
    CarritoFragment frCarritoFragment;
    PerfilFragment frPerfilFragment;
    FloatingActionButton fab;
    static public Globals globals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // setSupportActionBar(toolbar);

        frCatalogo = new Catalogo();
        frCatalogo.setOnEventListener(this);
        frCarritoFragment = new CarritoFragment();
        frPerfilFragment = new PerfilFragment();
        globals = new Globals(this);
        //fragmentP.setOnEventListener(PrincipalActivity.this);

        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.content_principal, frCatalogo);
        transaction.commit();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.dialog_menu);
                dialog.setTitle("Datos personales");

                LinearLayout btn_addtarjeta = (LinearLayout) dialog.findViewById(R.id.btn_addtarjeta);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                //lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.show();
                dialog.getWindow().setAttributes(lp);

                btn_addtarjeta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = new Dialog(MainActivity.this);
                        dialog.setContentView(R.layout.dialog_add_tarjeta);
                        dialog.setTitle("Datos personales");

                        LinearLayout btn_addtarjeta = (LinearLayout) dialog.findViewById(R.id.btn_addtarjeta);
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        //lp.height = WindowManager.LayoutParams.MATCH_PARENT;
                        dialog.show();
                        dialog.getWindow().setAttributes(lp);
                    }
                });
            }
        });

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onObjectListEvent(Object obj) {
        Log.e("Obj",obj+"");
        if(obj instanceof String){
            String accion = (String) obj;
            if(accion.equals("ProductoFragment")){
                ProductoFragment fragmentP = new ProductoFragment();
                fragmentP.setOnEventListener(MainActivity.this);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_principal, fragmentP);
                transaction.commit();
            }else if(accion.equals("carrito")){
                CarritoFragment fragmentC = new CarritoFragment();
                fragmentC.setOnEventListener(MainActivity.this);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_principal, fragmentC);
                transaction.commit();
                fab.setVisibility(View.GONE);
            }else if(accion.equals("pagar")){
                Toast.makeText(MainActivity.this,"EL pago fue realizado exitosamente",Toast.LENGTH_LONG).show();

                //fragmentP.setOnEventListener(MainActivity.this);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_principal, frCatalogo);
                transaction.commit();
                fab.setVisibility(View.GONE);
            }else if(accion.equals("Maps")){
                MapsFragment fragmentM = new MapsFragment();
                //fragmentP.setOnEventListener(MainActivity.this);
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.content_principal, fragmentM);
                transaction.commit();
                fab.setVisibility(View.GONE);
            }
        }
    }
}
