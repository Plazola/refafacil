package com.refafacil.zdp.refafacilv2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.refafacil.zdp.refafacilv2.R;
import com.refafacil.zdp.refafacilv2.events.ItemEvent;
import com.refafacil.zdp.refafacilv2.items.Producto;

import java.util.ArrayList;

public class ProductosAdapter extends RecyclerView.Adapter<ProductosAdapter.ViewHolder>{
    public ItemEvent objectListEvent;
    ArrayList<Producto> dataSet;
    Context context;

    public  void setOnEventListener(ItemEvent listener) {
        objectListEvent = listener;
    }

    public ProductosAdapter(ArrayList<Producto> dataSet, Context context) {
        this.dataSet = dataSet;
        this.context = context;
    }

    @Override
    public ProductosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_categoria, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.tv_nombre.setText(dataSet.get(position).getNombre());
        holder.tv_productos2.setText(dataSet.get(position).getNombre());
        holder.list_container.setTag(dataSet.get(position));

        holder.item_imagen.setImageResource(dataSet.get(position).getImagen());
        holder.item_imagen2.setImageResource(dataSet.get(position).getImagen());

        holder.list_container.setTag(dataSet.get(position));

        if(position%2==0){
            holder.lv_arriba.setVisibility(View.VISIBLE);
            holder.lv_abajo.setVisibility(View.GONE);
        }else{
            holder.lv_arriba.setVisibility(View.GONE);
            holder.lv_abajo.setVisibility(View.VISIBLE);
        }

        holder.list_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectListEvent.onObjectListEvent(v.getTag());
            }
        });

    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Atributos
        CheckBox cb_adicional;
        TextView tv_nombre,tv_productos2;
        ImageView item_imagen,item_imagen2;
        LinearLayout list_container,lv_abajo,lv_arriba;
        public ViewHolder(View view) {
            super(view);
            tv_nombre = (TextView) itemView.findViewById(R.id.tv_productos);
            tv_productos2 = (TextView) itemView.findViewById(R.id.tv_productos2);
            list_container = (LinearLayout) itemView.findViewById(R.id.list_container);
            list_container = (LinearLayout) itemView.findViewById(R.id.list_container);
            lv_arriba = (LinearLayout) itemView.findViewById(R.id.lv_arriba);
            lv_abajo = (LinearLayout) itemView.findViewById(R.id.lv_abajo);
            item_imagen = (ImageView) itemView.findViewById(R.id.item_imagen);
            item_imagen2 = (ImageView) itemView.findViewById(R.id.item_imagen2);
        }
    }


}
