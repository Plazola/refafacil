package com.refafacil.zdp.refafacilv2.items;

public class Carrito {
    int cantidad = 0;
    float precio = 0.0f;
    String nombre = "",marca = "";

    public Carrito(int cantidad, String nombre,float precio,String marca){
        this.cantidad = cantidad;
        this.marca = marca;
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public String getMarca() {
        return marca;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}
