package com.refafacil.zdp.refafacilv2.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.refafacil.zdp.refafacilv2.R;
import com.refafacil.zdp.refafacilv2.adapters.CategoriasAdapter;
import com.refafacil.zdp.refafacilv2.adapters.ProductosAdapter;
import com.refafacil.zdp.refafacilv2.adapters.ViewPagerAdapter;
import com.refafacil.zdp.refafacilv2.events.ItemEvent;
import com.refafacil.zdp.refafacilv2.items.Categoria;
import com.refafacil.zdp.refafacilv2.items.Producto;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Busqueda.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Busqueda#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Busqueda extends Fragment implements ItemEvent{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView recycler;
    RecyclerView recyclerView2;
    CategoriasAdapter adapter;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.LayoutManager layoutManager2;
    public ItemEvent objectListEvent;
    ViewPager viewPager;


    RecyclerView mListView;
    TextView mEmptyView;
    EditText et_buscar;
    ArrayList<Categoria> categoriasLista;
    ArrayList<Producto> productoFragmentLista;
    ArrayList<Busqueda> busquedaLista;
    ImageView b_carrito, b_busqueda;

    private OnFragmentInteractionListener mListener;
    public  void setOnEventListener(ItemEvent listener) {
        objectListEvent = listener;
    }
    public Busqueda() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Busqueda.
     */
    // TODO: Rename and change types and number of parameters
    public static Busqueda newInstance(String param1, String param2) {
        Busqueda fragment = new Busqueda();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_buscar, container, false);

        categoriasLista = new ArrayList<>();
        busquedaLista = new ArrayList<>();

       // categoriasLista.add(new Categoria("Productos",R.mipmap.productos));
       // categoriasLista.add(new Categoria("Marca",R.mipmap.marcas));
       // categoriasLista.add(new Categoria("Modelos",R.mipmap.productos));
       // categoriasLista.add(new Categoria("Suspensión",R.mipmap.marcas));


        mListView = (RecyclerView) view.findViewById(R.id.rv_categorias);
        b_carrito = (ImageView) view.findViewById(R.id.b_carrito);
        b_busqueda = (ImageView) view.findViewById(R.id.b_busqueda);
        et_buscar = (EditText) view.findViewById(R.id.et_buscar);



        mListView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());


        mListView.setLayoutManager(layoutManager);


        Log.e("KEY EVENT", "Do something");

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getContext());
        viewPager.setAdapter(viewPagerAdapter);

        recycler = (RecyclerView) view.findViewById(R.id.rv_categorias);
        recyclerView2 = (RecyclerView) view.findViewById(R.id.rv_productos);
        recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new CategoriasAdapter(categoriasLista, getContext());
        adapter.setOnEventListener(Busqueda.this);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);

        b_carrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objectListEvent.onObjectListEvent("carrito");
            }
        });

        b_busqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objectListEvent.onObjectListEvent("Maps");
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onObjectListEvent(Object obj) {
        if (obj instanceof Categoria) {
            Categoria cO = (Categoria) obj;

            productoFragmentLista = new ArrayList<>();
            productoFragmentLista.add(new Producto("Frenos",R.mipmap.productos));
            productoFragmentLista.add(new Producto("Suspensión",R.mipmap.marcas));
            productoFragmentLista.add(new Producto("Químicos",R.mipmap.quimicos));
            productoFragmentLista.add(new Producto("Sistema de enfriamiento",R.mipmap.enfriamiento));
            productoFragmentLista.add(new Producto("Sistema electrico",R.mipmap.electrico));
            productoFragmentLista.add(new Producto("Motor",R.mipmap.motor));
            productoFragmentLista.add(new Producto("Insumos",R.mipmap.insumos));

            ProductosAdapter adapter = new ProductosAdapter(productoFragmentLista, getContext());
            adapter.setOnEventListener(Busqueda.this);
            recyclerView2.setLayoutManager(layoutManager);
            recyclerView2.setAdapter(adapter);
            recyclerView2.setVisibility(View.VISIBLE);
            recycler.setVisibility(View.GONE);
            Log.e("change","Listas");

            layoutManager2 = new LinearLayoutManager(getContext());
            recyclerView2.setHasFixedSize(true);
            layoutManager2 = new LinearLayoutManager(getContext());
            recyclerView2.setLayoutManager(layoutManager2);
            recyclerView2.setAdapter(adapter);


        }else if(obj instanceof Producto){
            ArrayList<ProductoFragment> productoFragmentLista = new ArrayList<>();
            objectListEvent.onObjectListEvent("ProductoFragment");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
