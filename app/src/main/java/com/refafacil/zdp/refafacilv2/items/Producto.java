package com.refafacil.zdp.refafacilv2.items;

public class Producto {
    String nombre;
    int imagen;
    boolean visible = true;

    public Producto(String nombre,int imagen){
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public int getImagen() {
        return imagen;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
