package com.refafacil.zdp.refafacilv2.items;

public class Taller {
    String Nombre;
    int Id, calificacion = 3;
    long lnt,lat;

    public Taller(int Id, String Nombre,int calificacion){
        this.Id = Id;
        this.Nombre = Nombre;
        this.calificacion = calificacion;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId() {
        return Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public long getLat() {
        return lat;
    }

    public long getLnt() {
        return lnt;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public void setLnt(long lnt) {
        this.lnt = lnt;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }
}
