package com.refafacil.zdp.refafacilv2.items;

public class Modelo {
    String Nombre;
    int Id, Id_marca,Imagen;
    public Modelo(int Id,String Nombre,int Imagen,int Id_marca){
        this.Id = Id;
        this.Nombre = Nombre;
        this.Imagen = Imagen;
        this.Id_marca = Id_marca;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId() {
        return Id;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }

    public int getImagen() {
        return Imagen;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getId_marca() {
        return Id_marca;
    }

    public void setId_marca(int id_marca) {
        Id_marca = id_marca;
    }
}
