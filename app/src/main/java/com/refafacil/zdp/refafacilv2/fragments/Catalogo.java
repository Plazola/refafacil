package com.refafacil.zdp.refafacilv2.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.refafacil.zdp.refafacilv2.MainActivity;
import com.refafacil.zdp.refafacilv2.R;
import com.refafacil.zdp.refafacilv2.adapters.CategoriasAdapter;
import com.refafacil.zdp.refafacilv2.adapters.MarcaAdapter;
import com.refafacil.zdp.refafacilv2.adapters.ModeloAdapter;
import com.refafacil.zdp.refafacilv2.adapters.PiezaAdapter;
import com.refafacil.zdp.refafacilv2.adapters.ProductosAdapter;
import com.refafacil.zdp.refafacilv2.events.ItemEvent;
import com.refafacil.zdp.refafacilv2.items.Categoria;
import com.refafacil.zdp.refafacilv2.items.Marca;
import com.refafacil.zdp.refafacilv2.items.Modelo;
import com.refafacil.zdp.refafacilv2.items.Producto;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Catalogo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Catalogo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Catalogo extends Fragment implements ItemEvent{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView rv_marcas,rv_modelo,rv_categorias,rv_piezas;
    LinearLayout lv_marcas,lv_modelo,lv_categoria,lv_piezas,tag1,tag2,tag3;
    ImageView iv_marca,iv_modelo,iv_categoria,iv_pieza;
    TextView tagt1,tagt2,tagt3;
    public ItemEvent objectListEvent;

    private OnFragmentInteractionListener mListener;
    public  void setOnEventListener(ItemEvent listener) {
        objectListEvent = listener;
    }

    public Catalogo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Catalogo.
     */
    // TODO: Rename and change types and number of parameters
    public static Catalogo newInstance(String param1, String param2) {
        Catalogo fragment = new Catalogo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_catalogo, container, false);

        tag1 = view.findViewById(R.id.tag1);
        tag2 = view.findViewById(R.id.tag2);
        tag3 = view.findViewById(R.id.tag3);


        tagt1 = view.findViewById(R.id.tagt1);
        tagt2 = view.findViewById(R.id.tagt2);
        tagt3 = view.findViewById(R.id.tagt3);



        rv_marcas = view.findViewById(R.id.rv_marcas);
        rv_marcas.setHasFixedSize(true);
        LinearLayoutManager layoutMarca = new LinearLayoutManager(getContext());
        rv_marcas.setLayoutManager(layoutMarca);
        MarcaAdapter marcaAdapter = new MarcaAdapter(MainActivity.globals.listMarcas, getContext());
        marcaAdapter.setOnEventListener(this);
        rv_marcas.setAdapter(marcaAdapter);

        rv_categorias = view.findViewById(R.id.rv_categorias);
        rv_categorias.setHasFixedSize(true);
        LinearLayoutManager layoutCategoria = new LinearLayoutManager(getContext());
        rv_categorias.setLayoutManager(layoutCategoria);
        CategoriasAdapter categoriasAdapter = new CategoriasAdapter(MainActivity.globals.listCategorias, getContext());
        categoriasAdapter.setOnEventListener(this);
        rv_categorias.setAdapter(categoriasAdapter);

        rv_modelo = view.findViewById(R.id.rv_vehiculos);
        rv_modelo.setHasFixedSize(true);
        LinearLayoutManager layoutVehiculo = new LinearLayoutManager(getContext());
        rv_modelo.setLayoutManager(layoutVehiculo);
        ModeloAdapter modeloAdapter = new ModeloAdapter(MainActivity.globals.listModelos, getContext());
        modeloAdapter.setOnEventListener(this);
        rv_modelo.setAdapter(modeloAdapter);

        rv_piezas = view.findViewById(R.id.rv_piezas);
        rv_piezas.setHasFixedSize(true);
        LinearLayoutManager layoutPieza = new LinearLayoutManager(getContext());
        rv_piezas.setLayoutManager(layoutPieza);
        PiezaAdapter piezaAdapter = new PiezaAdapter(MainActivity.globals.listPieza, getContext());
        piezaAdapter.setOnEventListener(this);
        rv_piezas.setAdapter(piezaAdapter);

        lv_marcas =  view.findViewById(R.id.lv_marcas);
        lv_modelo =  view.findViewById(R.id.lv_modelo);
        lv_categoria = view.findViewById(R.id.lv_categoria);
        lv_piezas = view.findViewById(R.id.lv_piezas);

        iv_marca =  view.findViewById(R.id.iv_marca);
        iv_modelo = view.findViewById(R.id.iv_modelo);
        iv_categoria = view.findViewById(R.id.iv_categoria);
        iv_pieza = view.findViewById(R.id.iv_pieza);

        lv_marcas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_marca.setImageResource(R.mipmap.icon_marcas);
                iv_modelo.setImageResource(R.mipmap.icon_modelo_g);
                iv_categoria.setImageResource(R.mipmap.icon_taller_g);
                iv_pieza.setImageResource(R.mipmap.icon_pieza_g);

                rv_marcas.setVisibility(View.VISIBLE);
                rv_modelo.setVisibility(View.GONE);
                rv_categorias.setVisibility(View.GONE);
                rv_piezas.setVisibility(View.GONE);
            }
        });
        lv_modelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_marca.setImageResource(R.mipmap.icon_marcas_g);
                iv_modelo.setImageResource(R.mipmap.icon_modelo);
                iv_categoria.setImageResource(R.mipmap.icon_taller_g);
                iv_pieza.setImageResource(R.mipmap.icon_pieza_g);

                rv_marcas.setVisibility(View.GONE);
                rv_modelo.setVisibility(View.VISIBLE);
                rv_categorias.setVisibility(View.GONE);
                rv_piezas.setVisibility(View.GONE);
            }
        });
        lv_categoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_marca.setImageResource(R.mipmap.icon_marcas_g);
                iv_modelo.setImageResource(R.mipmap.icon_modelo_g);
                iv_categoria.setImageResource(R.mipmap.icon_taller);
                iv_pieza.setImageResource(R.mipmap.icon_pieza_g);

                rv_marcas.setVisibility(View.GONE);
                rv_modelo.setVisibility(View.GONE);
                rv_categorias.setVisibility(View.VISIBLE);
                rv_piezas.setVisibility(View.GONE);
            }
        });
        lv_piezas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_marca.setImageResource(R.mipmap.icon_marcas_g);
                iv_modelo.setImageResource(R.mipmap.icon_modelo_g);
                iv_categoria.setImageResource(R.mipmap.icon_taller_g);
                iv_pieza.setImageResource(R.mipmap.icon_pieza);

                rv_marcas.setVisibility(View.GONE);
                rv_modelo.setVisibility(View.GONE);
                rv_categorias.setVisibility(View.GONE);
                rv_piezas.setVisibility(View.VISIBLE);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onObjectListEvent(Object obj) {

        if (obj instanceof Marca) {
            Marca mO = (Marca) obj;

            tagt1.setText(mO.getNombre());
            tag1.setVisibility(View.VISIBLE);
            iv_marca.setImageResource(R.mipmap.icon_marcas_g);
            iv_modelo.setImageResource(R.mipmap.icon_modelo);
            iv_categoria.setImageResource(R.mipmap.icon_taller_g);
            iv_pieza.setImageResource(R.mipmap.icon_pieza_g);

            rv_marcas.setVisibility(View.GONE);
            rv_modelo.setVisibility(View.VISIBLE);
            rv_categorias.setVisibility(View.GONE);
            rv_piezas.setVisibility(View.GONE);

        }else if (obj instanceof Modelo) {
            Modelo mO = (Modelo) obj;
            tagt2.setText(mO.getNombre());
            tag2.setVisibility(View.VISIBLE);
            iv_marca.setImageResource(R.mipmap.icon_marcas_g);
            iv_modelo.setImageResource(R.mipmap.icon_modelo_g);
            iv_categoria.setImageResource(R.mipmap.icon_taller);
            iv_pieza.setImageResource(R.mipmap.icon_pieza_g);

            rv_marcas.setVisibility(View.GONE);
            rv_modelo.setVisibility(View.GONE);
            rv_categorias.setVisibility(View.VISIBLE);
            rv_piezas.setVisibility(View.GONE);

        }else if (obj instanceof Categoria) {
            Categoria cO = (Categoria) obj;

            tagt3.setText(cO.getNombre());
            tag3.setVisibility(View.VISIBLE);
            iv_marca.setImageResource(R.mipmap.icon_marcas_g);
            iv_modelo.setImageResource(R.mipmap.icon_modelo_g);
            iv_categoria.setImageResource(R.mipmap.icon_taller_g);
            iv_pieza.setImageResource(R.mipmap.icon_pieza);

            rv_marcas.setVisibility(View.GONE);
            rv_modelo.setVisibility(View.GONE);
            rv_categorias.setVisibility(View.GONE);
            rv_piezas.setVisibility(View.VISIBLE);
        }else if(obj instanceof Producto){
            Producto pO = (Producto) obj;
            ArrayList<ProductoFragment> productoFragmentLista = new ArrayList<>();
            objectListEvent.onObjectListEvent("ProductoFragment");
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
