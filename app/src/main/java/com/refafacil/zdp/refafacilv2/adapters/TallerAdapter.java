package com.refafacil.zdp.refafacilv2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.refafacil.zdp.refafacilv2.R;
import com.refafacil.zdp.refafacilv2.events.ItemEvent;
import com.refafacil.zdp.refafacilv2.items.Carrito;
import com.refafacil.zdp.refafacilv2.items.Taller;

import java.util.ArrayList;
import java.util.List;

public class TallerAdapter extends RecyclerView.Adapter<TallerAdapter.ViewHolder>{
    public ItemEvent objectListEvent;
     static List<Taller> dataSet;
    Context context;

    public  void setOnEventListener(ItemEvent listener) {
        objectListEvent = listener;
    }

    public TallerAdapter(List<Taller> dataSet, Context context) {
        //this.dataSet = dataSet;
        this.dataSet = new ArrayList<>(dataSet);
        this.context = context;
    }

    @Override
    public TallerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_taller, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final  Taller Object = dataSet.get(position);
        holder.tv_productos.setText(Object.getNombre());
        holder.list_container.setTag(Object);
        holder.list_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectListEvent.onObjectListEvent(v.getTag());
            }
        });

    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Atributos
        TextView tv_productos,tv_numero,tv_modelo,tv_precio;
        LinearLayout list_container;
        public ViewHolder(View view) {
            super(view);
            tv_productos = (TextView) itemView.findViewById(R.id.tv_productos);
            tv_numero = (TextView) itemView.findViewById(R.id.tv_numero);
            tv_modelo = (TextView) itemView.findViewById(R.id.tv_modelo);
            tv_precio = (TextView) itemView.findViewById(R.id.tv_precio);
            list_container = (LinearLayout) itemView.findViewById(R.id.list_container);
        }
    }

    //Search
    public void animateTo(List<Taller> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Taller> newModels) {
        for (int i = dataSet.size() - 1; i >= 0; i--) {
            final Taller model = dataSet.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }

        }
    }

    private void applyAndAnimateAdditions(List<Taller> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Taller model = newModels.get(i);
            if (!dataSet.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Taller> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Taller model = newModels.get(toPosition);
            final int fromPosition = dataSet.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Taller removeItem(int position) {
        final Taller model = dataSet.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Taller model) {
        dataSet.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Taller model = dataSet.remove(fromPosition);
        dataSet.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }
    // */

}
